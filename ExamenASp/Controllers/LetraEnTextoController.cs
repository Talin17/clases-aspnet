﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenASp.Controllers
{
    public class LetraEnTextoController : Controller
    {
        // GET: LetraEnTexto
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult letraRepetida()
        {
                string texto = "Un heroe no es mas valiente que cualquier otra persona, solamente es valiente cinco minutos mas.";
                int count = 0;

                foreach (char contar in texto)
                {
                    if (contar == 'a') count++;
                }
                ViewBag.mensaje = $"La letra A se repite {count} veces";

                return View();
        }
    }
}