﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenASp.Controllers
{
    public class RestpDivController : Controller
    {
        // GET: RestpDiv
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult restoDiv(int v1, int v2)
        {
            float result = 0;
            ViewBag.mensaje = "";
            if (v1 <= 0 || v2 <= 0)
            {
                ViewBag.mensaje = "No es posible realizar la operacion.";
                return View();
                
            }else
            {
                result = v1 % v2;
            }

            return Content($"El resto de la division de {v1} y {v2} es {result}");
        }
    }
}