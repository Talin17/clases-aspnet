﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenASp.Controllers
{
    public class NotaEstudianteController : Controller
    {
        // GET: NotaEstudiante
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult estudiante(string nombre, int parcial1, int parcial2, int parcial3)
        {
            int sumaNotas = parcial1 + parcial2 + parcial3;
            int promedio = sumaNotas / 3;
            ViewBag.mensaje = "";
            if (promedio >=70)
            {
                ViewBag.mensaje = $"El alumno {nombre} aprobo!";
            }else
            {
                ViewBag.mensaje = $"El alumno {nombre} no aprobo!";
            }
            return View();

        }
    }
}