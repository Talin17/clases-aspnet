﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AulaswithDB.Models;

namespace AulaswithDB.Controllers
{
    public class aulasController : Controller
    {
        private aulasEntities db = new aulasEntities();

        // GET: aulas
        public ActionResult Index()
        {
            var aula = db.aula.Include(a => a.talleres).Include(a => a.tipoAula);
            return View(aula.ToList());
        }

        // GET: aulas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aula aula = db.aula.Find(id);
            if (aula == null)
            {
                return HttpNotFound();
            }
            return View(aula);
        }

        // GET: aulas/Create
        public ActionResult Create()
        {
            ViewBag.tallerID = new SelectList(db.talleres, "id", "nombreTaller");
            ViewBag.tipoID = new SelectList(db.tipoAula, "id", "nombreTipo");
            return View();
        }

        // POST: aulas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,tipoID,tallerID,nombreAula,capacidad")] aula aula)
        {
            if (ModelState.IsValid)
            {
                db.aula.Add(aula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.tallerID = new SelectList(db.talleres, "id", "nombreTaller", aula.tallerID);
            ViewBag.tipoID = new SelectList(db.tipoAula, "id", "nombreTipo", aula.tipoID);
            return View(aula);
        }

        // GET: aulas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aula aula = db.aula.Find(id);
            if (aula == null)
            {
                return HttpNotFound();
            }
            ViewBag.tallerID = new SelectList(db.talleres, "id", "nombreTaller", aula.tallerID);
            ViewBag.tipoID = new SelectList(db.tipoAula, "id", "nombreTipo", aula.tipoID);
            return View(aula);
        }

        // POST: aulas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,tipoID,tallerID,nombreAula,capacidad")] aula aula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tallerID = new SelectList(db.talleres, "id", "nombreTaller", aula.tallerID);
            ViewBag.tipoID = new SelectList(db.tipoAula, "id", "nombreTipo", aula.tipoID);
            return View(aula);
        }

        // GET: aulas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aula aula = db.aula.Find(id);
            if (aula == null)
            {
                return HttpNotFound();
            }
            return View(aula);
        }

        // POST: aulas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            aula aula = db.aula.Find(id);
            db.aula.Remove(aula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
