﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Periodico1.Models;

namespace Periodico1.Controllers
{
    public class noticiasController : Controller
    {
        private db_periodicoEntities db = new db_periodicoEntities();

        // GET: noticias
        public ActionResult Index()
        {
            var noticias = db.noticias.Include(n => n.categorias);
            return View(noticias.ToList());
        }

        // GET: noticias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            noticias noticias = db.noticias.Find(id);
            if (noticias == null)
            {
                return HttpNotFound();
            }
            return View(noticias);
        }

        // GET: noticias/Create
        public ActionResult Create()
        {
            ViewBag.categoria_id = new SelectList(db.categorias, "id", "categoria");
            return View();
        }

        // POST: noticias/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,categoria_id,titulo,detalle_completo,detalle_corto,fecha,imagen,ruta_imagen")] noticias noticias)
        {
            if (ModelState.IsValid)
            {
                db.noticias.Add(noticias);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.categoria_id = new SelectList(db.categorias, "id", "categoria", noticias.categoria_id);
            return View(noticias);
        }

        // GET: noticias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            noticias noticias = db.noticias.Find(id);
            if (noticias == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoria_id = new SelectList(db.categorias, "id", "categoria", noticias.categoria_id);
            return View(noticias);
        }

        // POST: noticias/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,categoria_id,titulo,detalle_completo,detalle_corto,fecha,imagen,ruta_imagen")] noticias noticias)
        {
            if (ModelState.IsValid)
            {
                db.Entry(noticias).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoria_id = new SelectList(db.categorias, "id", "categoria", noticias.categoria_id);
            return View(noticias);
        }

        // GET: noticias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            noticias noticias = db.noticias.Find(id);
            if (noticias == null)
            {
                return HttpNotFound();
            }
            return View(noticias);
        }

        // POST: noticias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            noticias noticias = db.noticias.Find(id);
            db.noticias.Remove(noticias);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
