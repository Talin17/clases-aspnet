﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class RandomParametroController : Controller
    {
        // GET: RandomParametro
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult randomParametro1 (int valor1)
        {
            ViewBag.mensaje = "";
            string concat = "";
            if (valor1 >=50 && valor1 <=100)
            {
                
              
                int[] array = new int[30];
                Random numeroRandom = new Random();
                int numero = 0;
                for (int i = 0; i < array.Length; i++)
                {
                    numero = numeroRandom.Next(50, 101);
                    array[i] = numero;

                    concat += $"Posicion {i} numero random = {array[i]}";
                    

                }
                ViewBag.mensaje = $"Parametro aceptado!{concat}";

            }
            else
            {
                ViewBag.mensaje = "Numero fuera de los parametros!";
            }
            return View();
        }
    }
}