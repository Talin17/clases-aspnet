﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class TablaDel6Controller : Controller
    {
        // GET: TablaDel6
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult tablaMulti()
        {
            string concact = "";
            int multiplicador = 6;
            int resultado = 0;
            for (int i = 0; i <=12; i++)
            {
                resultado = i * multiplicador;
                concact += $"{multiplicador} x {i} = {resultado}<br/>";
            }
            return Content(concact);
            
        }
    }
}