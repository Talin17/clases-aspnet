﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class NumeroRandomMatrizController : Controller
    {
        // GET: NumeroRandomMatriz
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult matrizRandom()
        {
            string concat = "";
            int[] array = new int[15];
            Random numeroRandom = new Random();
            int numero = 0;
            for (int i = 0; i <array.Length ; i++)
            {
                numero = numeroRandom.Next(1, 101);
                array[i] = numero;

                concat += $"Posicion {i} numero random = {array[i]}<br/>";
            }
            return Content(concat);
        }
    }
}