﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class TablaPorTecladoController : Controller
    {
        // GET: TablaPorTeclado
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult porTeclado(int introducir)
        {
            string concact = "";
            int resultado = 0;
            for (int i = 0; i <= 12; i++)
            {
                resultado = i * introducir;
                concact += $"{introducir} x {i} = {resultado}<br/>";
            }
            return Content(concact);
        }
    }
}