﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class NotaFinalController : Controller
    {
        // GET: NotaFinal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult notaFinal1(int nota)
        {
            string diag = "";
            if(nota <= 69)
            {
                diag = $"Su nota final es: {nota} su calificacion es: D";
            }
            else if (nota >= 70 && nota <= 79)
            {
                diag = $"Su nota final es: {nota} su calificacion es: C";
            }
            else if (nota >=80 && nota <= 89)
            {
                diag = $"Su nota final es: {nota} su calificacion es: B";
            }
            else if (nota >= 90 && nota <= 100)
            {
                diag = $"Su nota final es: {nota} su calificacion es: A";
            }
            return Content(diag);
        }
    }
}