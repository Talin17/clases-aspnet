﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class NumeroRandom100Controller : Controller
    {
        // GET: NumeroRandom100
        public ActionResult Index()
        {
            ViewBag.numero = new Random().Next(1,101);
            return View();
        }


    }
}