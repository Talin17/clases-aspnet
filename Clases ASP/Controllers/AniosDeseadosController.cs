﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class AniosDeseadosController : Controller
    {
        // GET: AniosDeseados
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult aniosDeseados(int deseados)
        {
            string concact = "";
            float interes = 15067.5f;
            float calculo = 0;
            for (int i = 1; i <= deseados ; i++)
            {
                calculo = i * interes;
                concact += $"Inversion $100,450: interes {interes} : interes generado {calculo} por {i} años<br/>";
            }
            return Content(concact);
        }
    }
}