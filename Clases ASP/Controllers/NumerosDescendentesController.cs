﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class NumerosDescendentesController : Controller
    {
        // GET: NumerosDescendentes
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult numerosDescendentes()
        {
            string show = "";
            for (int i=2022; i>=1900; i--)
            {
                show = show + "\n" + i.ToString();
                
            }
            return Content($"Numeros: {show} ");
        }
    }
}