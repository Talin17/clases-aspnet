﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class PromedioController : Controller
    {
        // GET: Promedio
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Promedio1(int nota1, int nota2, int nota3, int nota4)
        {
            string diag = "";

            int sumaTotal = 0;
            sumaTotal = nota1 + nota2 + nota3 + nota4;
           int promedioNota = sumaTotal / 4;
            if (promedioNota > 69)
            {
                diag = $"Su promedio es: {promedioNota} usted aprobo!";

            }else if (promedioNota < 70)
            {
                diag = $"Su promedio es: {promedioNota} usted reprobo!";
            }

            return Content(diag);
        }
    }
}