﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class LoginInfotepController : Controller
    {
        // GET: LoginInfotep
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult login(string usuario, string clave)
        {
            ViewBag.mensaje = "";
            if (usuario == "infotep" && clave =="transparencia")
            {
                ViewBag.mensaje = $"Bienvenido {usuario}";
            }
            else
            {
                ViewBag.mensaje = $"El usuario {usuario} y/o contraseña son incorrectos";
            }
            return View();
        }
    }
}