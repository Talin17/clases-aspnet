﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectosASP.Controllers
{
    public class IngresarNotaController : Controller
    {
        // GET: IngresarNota
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ingresarNota(int nota1, int nota2, int nota3, int nota4)
        {
            string diag = "";
            int sumaNotas = nota1 + nota2 + nota3 + nota4;
            int notaFinal = sumaNotas / 4;
            
            if (notaFinal<= 69)
            {
                diag = $"Su nota final es: {notaFinal} su calificacion final es D";
            }
            else if (notaFinal >=70 && notaFinal <= 79)
            {
                diag = $"Su nota final es: {notaFinal} su calificacion final es C";
            }
            else if (notaFinal >=80 && notaFinal <=89) {
                diag = $"Su nota final es: {notaFinal} su calificacion final es B";
            }
            else if (notaFinal >=90 && notaFinal <=100)
            {
                diag = $"Su nota final es: {notaFinal} su calificacion final es A";
            }
            return Content(diag);
        }
    }
}