﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Libreria.Models;

namespace Libreria.Controllers
{
    public class librosController : Controller
    {
        private libreriaEntities db = new libreriaEntities();

        // GET: libros
        public ActionResult Index()
        {
            var libros = db.libros.Include(l => l.autor).Include(l => l.editorial).Include(l => l.pasillos);
            return View(libros.ToList());
        }

        // GET: libros/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            libros libros = db.libros.Find(id);
            if (libros == null)
            {
                return HttpNotFound();
            }
            return View(libros);
        }

        // GET: libros/Create
        public ActionResult Create()
        {
            ViewBag.autorID = new SelectList(db.autor, "id", "nombreAutor");
            ViewBag.editorialID = new SelectList(db.editorial, "id", "nombreEditorial");
            ViewBag.pasillosID = new SelectList(db.pasillos, "id", "pasillo");
            return View();
        }

        // POST: libros/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,autorID,editorialID,pasillosID,nombre")] libros libros)
        {
            if (ModelState.IsValid)
            {
                db.libros.Add(libros);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.autorID = new SelectList(db.autor, "id", "nombreAutor", libros.autorID);
            ViewBag.editorialID = new SelectList(db.editorial, "id", "nombreEditorial", libros.editorialID);
            ViewBag.pasillosID = new SelectList(db.pasillos, "id", "pasillo", libros.pasillosID);
            return View(libros);
        }

        // GET: libros/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            libros libros = db.libros.Find(id);
            if (libros == null)
            {
                return HttpNotFound();
            }
            ViewBag.autorID = new SelectList(db.autor, "id", "nombreAutor", libros.autorID);
            ViewBag.editorialID = new SelectList(db.editorial, "id", "nombreEditorial", libros.editorialID);
            ViewBag.pasillosID = new SelectList(db.pasillos, "id", "pasillo", libros.pasillosID);
            return View(libros);
        }

        // POST: libros/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,autorID,editorialID,pasillosID,nombre")] libros libros)
        {
            if (ModelState.IsValid)
            {
                db.Entry(libros).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.autorID = new SelectList(db.autor, "id", "nombreAutor", libros.autorID);
            ViewBag.editorialID = new SelectList(db.editorial, "id", "nombreEditorial", libros.editorialID);
            ViewBag.pasillosID = new SelectList(db.pasillos, "id", "pasillo", libros.pasillosID);
            return View(libros);
        }

        // GET: libros/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            libros libros = db.libros.Find(id);
            if (libros == null)
            {
                return HttpNotFound();
            }
            return View(libros);
        }

        // POST: libros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            libros libros = db.libros.Find(id);
            db.libros.Remove(libros);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
