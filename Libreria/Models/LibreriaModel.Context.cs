﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Libreria.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class libreriaEntities : DbContext
    {
        public libreriaEntities()
            : base("name=libreriaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<autor> autor { get; set; }
        public virtual DbSet<editorial> editorial { get; set; }
        public virtual DbSet<libros> libros { get; set; }
        public virtual DbSet<pasillos> pasillos { get; set; }
    }
}
